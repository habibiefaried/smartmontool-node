-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 25, 2014 at 05:39 AM
-- Server version: 5.5.35-0ubuntu0.13.10.2
-- PHP Version: 5.5.3-1ubuntu2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hdmon`
--

-- --------------------------------------------------------

--
-- Table structure for table `full_status_history`
--

CREATE TABLE IF NOT EXISTS `full_status_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_harddisk` int(11) NOT NULL,
  `status` text NOT NULL,
  `insert_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `harddisk`
--

CREATE TABLE IF NOT EXISTS `harddisk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mountpoint` varchar(100) NOT NULL,
  `id_host` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `harddisk`
--

INSERT INTO `harddisk` (`id`, `mountpoint`, `id_host`) VALUES
(1, '/dev/ada0', 1),
(2, '/dev/pass0', 1),
(3, '/dev/pass1', 1),
(4, '/dev/pass2', 1),
(5, '/dev/pass12', 1),
(6, '/dev/pass11', 1);

-- --------------------------------------------------------

--
-- Table structure for table `host`
--

CREATE TABLE IF NOT EXISTS `host` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `host`
--

INSERT INTO `host` (`id`, `host`) VALUES
(1, '167.205.23.90');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
