var shell = require("../routes/shell");
var model = require("../routes/model");
/*
 * GET home page.
 */

exports.index = function(req, res){
  model.getAllHD(function(results) {
  	res.render('index', {data:results});
  });
};

exports.currentstatus = function(req,res){
  var id = req.param('id_harddisk');
  model.getHDMountPoint(id,function(results) {
	  shell.getAllAttr("root",results[0].host,"smartctl -a "+results[0].mountpoint,function(retr) {
		res.send("<pre>"+retr+"</pre>");
	  });
  });
};

exports.addmount = function(req,res) {
	model.getAllHost(function(results) {
		res.render('addmount',{data:results});
	});
};

exports.addmountprocess = function(req,res){
	var id_host = req.param('host');
	var mountpoint = req.param('mountpoint');
	model.insertMountPoint(id_host,mountpoint,function(results) {
		res.redirect('/');	
	});
};

exports.addhost = function(req,res){
	res.render('addhost');
};

exports.addhostprocess = function(req,res) {
	var ip = req.param('host');
	model.insertHost(ip,function(results) {
		res.redirect('');
	});
};
