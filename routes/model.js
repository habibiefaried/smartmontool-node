/*===================================================================================================*/
/* Editable Configuration */
/*===================================================================================================*/

var user_db = "root"; //username database, silahkan diganti sesuai kondisi
var pass_db = "tkislam123"; //password database, silahkan diganti sesuai kondisi
var name_db = "hdmon"; //nama database yang akan digunakan, silahkan diganti sesuai kondisi 
var interval = 600; //berapa detik interval refresh (10 menit)

/*===================================================================================================*/
/* Non-editable Configuration */
/*===================================================================================================*/

var mysql = require('mysql');
var client = mysql.createConnection({user:user_db,password:pass_db,database:name_db});
exports.data = client;
exports.interval = interval;

/*===================================================================================================*/
/* Functions */
/*===================================================================================================*/

function getAllHost(callback) {
	client.query("SELECT * FROM host",function(err,results) {
		if (err) throw err;
		else callback(results);
	});
}


function getAllHD(callback) {
	client.query("SELECT * FROM host JOIN harddisk where host.id=harddisk.id_host order by host.host asc, harddisk.mountpoint asc",function(err,results) {
		if (err) throw err;
		else callback(results);
	});
}

function getHDMountPoint(id,callback) {
	client.query("SELECT * FROM host JOIN harddisk where host.id=harddisk.id_host AND harddisk.id="+id,function(err,results) {
		if (err) throw err;
		else callback(results);
	});
}

function insertMountPoint(id_host,mountpoint,callback) {
	client.query("INSERT INTO harddisk (id_host,mountpoint) VALUES ("+id_host+",'"+mountpoint+"')",function (err,results) {
		if (err) throw err;
		else callback(results);
	});
}

function insertHost(ip_address,callback) {
	client.query("INSERT INTO host (host) VALUES('"+ip_address+"')",function(err,results){
		if (err) throw err;
		else callback(results);	
	});
}
/*===================================================================================================*/
/* Exports */
/*===================================================================================================*/
exports.getAllHD = getAllHD;
exports.getHDMountPoint = getHDMountPoint;
exports.insertMountPoint = insertMountPoint;
exports.getAllHost = getAllHost;
exports.insertHost = insertHost;
